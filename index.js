function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    // Start writing code here

    if(letter.length === 1){
        for( let i = 0; i < sentence.length; i++){
            if(letter === sentence.charAt(i)){
                result += 1;
            }
        }
    }
    else{
        result = undefined;
    }

    return result;    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    // Start writing code here

    let arrText = text.toLowerCase().split('');
    let array = []
    for(let i = 0; i<arrText.length ; i++){
       
    	 if(arrText.includes(arrText[i], i+1)){
            array.push(arrText[i]);
        }
        
    }
    if(array.length === 0){
        return true;
    }
    else{
        return false
    }
    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    // Start writing code here
    
    if(age < 13 ){
            return undefined;
        }  
    else if(age > 12 && age <= 21){
            price = price - (price * 0.20);
            // let finalPrice = Math.round(price)
            return price.toFixed(2).toString();
        }
    else if(age > 21 && age <= 64){
            // let finalPrice = Math.round(price)
            return price.toFixed(2).toString();
        }
    else{
            price = price - (price * 0.20);
            // let finalPrice = Math.round(price)
            return price.toFixed(2).toString();
        } 
}


function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    // Start writing code here
    
    let array = [];

    for(i=0 ; i<items.length; i++){
        
        if(items[i].stocks === 0){
            if(array.includes(items[i].category)){
                
            }
            else{
                array.push(items[i].category)
            }
        }
       
    }

    return array;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    // Start writing code here
   let array = [];
    for(i = 0; i < candidateA.length; i++){
        if(candidateB.includes(candidateA[i])){
            array.push(candidateA[i]);
        }

    }

    return array;


}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};